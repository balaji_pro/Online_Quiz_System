## Online Quiz System
A web application used to conduct online quiz. It's composed of Admin and User modules.

 - ### Features:
  - Questions, Answer types, Answer options, Correct Answer(s), Exam timing, Time slots specific to questions can be configurable by Admin.
  - Instantaneous evaluation and result.
  - Maintains user regard/performance. 

 - ### Technology used:
  - HTML, PHP and MySQL.
 - <h4>Start at: src/home.php </h4>
 - ### Few Screenshots:
  - <h4>Home</h4>![home](/uploads/d620a205b29a8d6cf82151eb05854e26/home.png)
  - <h4>Signin</h4>![signin](/uploads/dfa84a20712dda02020a41d579667a38/signin.png)
  - <h4>Change pin</h4>![changepin](/uploads/aef0719a30e5024f36f1a591e5811941/changepin.png)
  - <h4>Add questions</h4>![question](/uploads/8c7a5498174811dc82d053cd1bba3352/question.png)
  - <h4>Set time</h4>![settime](/uploads/9a091d86391aa5b4357d724e0c585424/settime.png)
  - <h4>Exam</h4>![studque](/uploads/d19bfe5fe9062897a4f1d7d7b4aa23aa/studque.png)